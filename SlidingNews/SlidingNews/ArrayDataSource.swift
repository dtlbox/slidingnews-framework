//
//  ArrayDataSource.swift
//  SlidingNews
//
//  Created by Pavel Karpov on 23.09.16.
//  Copyright © 2016 Pavel Karpov. All rights reserved.
//

import UIKit
import Foundation

public class ArrayDataSource: NSObject {
    
    public var items                : [AnyObject]!
    public var tableView            : UITableView!
    var cellIdentifier              : String!
    var cellConfigureBlock          : ((AnyObject, AnyObject) -> ())!
    public var selectedIndexes      = [Int]()

    public init(cellIdentifier:String,
         andConfigureCellClosure configureCell: ((AnyObject, AnyObject) -> ())!) {
        
        self.cellIdentifier         = cellIdentifier
        self.cellConfigureBlock     = configureCell
    }
    
}

extension ArrayDataSource: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        let item = items[indexPath.row]
        
        self.cellConfigureBlock(cell,item)
        
        cell.layoutMargins  = UIEdgeInsets.zero
        cell.selectionStyle = .none
        return cell
    }
}

extension ArrayDataSource: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexes.contains(indexPath.row)
        {
            return UITableViewAutomaticDimension
        }else{
            return tableView.estimatedRowHeight
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexes.contains(indexPath.row){
            selectedIndexes.removeObject(object: indexPath.row)
        }else{
            selectedIndexes.append(indexPath.row)
        }
//        tableView.beginUpdates()
//        tableView.reloadRows(at: [indexPath], with: .automatic)
//        tableView.endUpdates()
        tableView.reloadDataAnimatedKeepingOffset()
    }
}
