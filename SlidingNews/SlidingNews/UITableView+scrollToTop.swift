//
//  UITableView+scrollToTop.swift
//  SlidingNews
//
//  Created by Pavel Karpov on 28.09.16.
//  Copyright © 2016 Pavel Karpov. All rights reserved.
//

extension UITableView
{
    func reloadDataAnimatedKeepingOffset()
    {
        let offset = contentOffset
        UIView.setAnimationsEnabled(false)
        beginUpdates()
        endUpdates()
        UIView.setAnimationsEnabled(true)
        layoutIfNeeded()
        contentOffset = offset
    }
}
