//
//  ViewController.swift
//  NewsExample
//
//  Created by Pavel Karpov on 21.09.16.
//  Copyright © 2016 Pavel Karpov. All rights reserved.
//

import UIKit

@objc public protocol SlidingNewsControllerDelegate: class {
    func willDisplayViewController(viewController: UITableViewController)
}

@objc public class SlidingNewsController: UIViewController {
  
    fileprivate enum Segue: String {
        case PageController = "PageControllerSegue"
        case PageIndex      = "PageIndexSegue"
    }
  
    fileprivate let ViewControllerIdentifier  = "TableViewController"
    fileprivate let startIndex                = 0
    
    //vc config
    var viewControllersCount    : Int!
    weak var delegate           : SlidingNewsControllerDelegate?
    var customCellIdentifier    : String!
    var bundle                  : String!
    var rowHeight               : CGFloat!
    var isCustomCellInitFromNib = true
    
    //tab bar config
    var tbBackgroundColor       : UIColor!
    var tbTextFont              : UIFont!
    var tbTextColor             : UIColor!
    var tbSelectedTextColor     : UIColor!
    var tbTabTitles             : [String]!
    var tbTabSpacing            : CGFloat!
  
    weak var pageController: PageController? {
        didSet {
          self.setControllersCoupling()
        }
    }
    weak var pageIndex: PageIndexCollectionViewController? {
        didSet {
          self.setControllersCoupling()
        }
    }
  
    public override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pageController?.goToIndex(startIndex)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.pageIndex?.collectionView.selectItem(at: IndexPath(row: self.startIndex, section: 1),
                                                      animated: true,
                                                      scrollPosition: .centeredHorizontally)
        }
    }
    
    @objc public class func factoryWithCell(fromNib:Bool,
                                      numberOfViewControllers: Int,
                                      delegate: SlidingNewsControllerDelegate,
                                      cellIdentifier: String,
                                      bundle: String,
                                      rowHeight: CGFloat) -> SlidingNewsController {
        
        let frameworkBundle         = Bundle(identifier: "fork-pork.SlidingNews")
        let sb                      = UIStoryboard(name: "SlidingNews", bundle: frameworkBundle)
        let vc                      = sb.instantiateViewController(withIdentifier: "SlidingNewsController") as! SlidingNewsController
        vc.viewControllersCount     = numberOfViewControllers
        vc.delegate                 = delegate
        vc.customCellIdentifier     = cellIdentifier
        vc.bundle                   = bundle
        vc.rowHeight                = rowHeight
        vc.isCustomCellInitFromNib  = fromNib
        return vc
    }
    
    public func configureTabBarWith(backgroundColor: UIColor,
                                          textFont: UIFont,
                                          textColor: UIColor,
                                          selectedTextColor: UIColor,
                                          tabSpacing: CGFloat,
                                          andTabTitles tabTitles: [String]){
        
        self.tbBackgroundColor      = backgroundColor
        self.tbTextFont             = textFont
        self.tbTextColor            = textColor
        self.tbSelectedTextColor    = selectedTextColor
        self.tbTabTitles            = tabTitles
        self.tbTabSpacing           = tabSpacing
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        guard let identifier = segue.identifier else {
            return
        }
        guard let segueEnum = Segue(rawValue: identifier) else {
            return
        }

        switch segueEnum {
        case .PageController:
            guard let pageController = segue.destination as? PageController else {
                break
            }
          
            self.pageController         = pageController
            pageController.dataSource   = self
            pageController.delegate     = self
        case .PageIndex:
            guard let pageIndex = segue.destination as? PageIndexCollectionViewController else {
                break
            }
          
            self.pageIndex              = pageIndex
            pageIndex.dataSource        = self
            pageIndex.backgroundColor   = self.tbBackgroundColor
            pageIndex.tabSpacing        = self.tbTabSpacing
        }
    }
  
    fileprivate func setControllersCoupling() {
        pageController?.pageIndexController = self.pageIndex
        pageIndex?.pageController           = self.pageController
    }
}

extension SlidingNewsController: PageControllerDataSource {
    public func viewControllerAtIndex(_ index: Int) -> UIViewController {
        print("Loading page at index \(index)")
        let frameworkBundle         = Bundle(identifier: "fork-pork.SlidingNews")
        let storyboard              = UIStoryboard(name: "SlidingNews", bundle: frameworkBundle)
        let viewControllerToReturn  = storyboard.instantiateViewController(withIdentifier: ViewControllerIdentifier) as! UITableViewController
        
        viewControllerToReturn.tableView.tag = index
        self.configTableView(tableView: viewControllerToReturn.tableView)
        return viewControllerToReturn
    }
    
    public func numberOfViewControllers() -> Int {
        return self.viewControllersCount
    }
    
    func configTableView (tableView: UITableView){
        if self.isCustomCellInitFromNib {
            tableView.register(UINib(nibName: self.customCellIdentifier,
                                     bundle: Bundle(identifier: self.bundle)),
                               forCellReuseIdentifier: self.customCellIdentifier)
        } else {
            let cellClass = NSClassFromString(self.bundle.components(separatedBy: ".").last! + "." + self.customCellIdentifier) as! UITableViewCell.Type
            tableView.register(cellClass, forCellReuseIdentifier: self.customCellIdentifier)
        }
        tableView.layoutMargins         = UIEdgeInsets.zero
        tableView.separatorInset        = UIEdgeInsets.zero
        tableView.estimatedRowHeight    = rowHeight
    }
}

extension SlidingNewsController: PageControllerDelegate {
    public func willDisplayController(viewController: UITableViewController) {
        guard let delegate = self.delegate else {
            return
        }
        delegate.willDisplayViewController(viewController: viewController)
    }
}

extension SlidingNewsController: PageIndexCollectionViewControllerDataSource {
    public func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell",
                                                      for: indexPath) as! IndexCollectionViewCell
        
        cell.indexLabel.text        = self.tbTabTitles[indexPath.row]
        cell.indexLabel.textColor   = self.tbTextColor
        cell.indexLabel.font        = self.tbTextFont
        cell.textColor              = self.tbTextColor
        cell.selectedTextColor      = self.tbSelectedTextColor
        
        cell.indexLabel.preferredMaxLayoutWidth = 50
        return cell
    }
}
