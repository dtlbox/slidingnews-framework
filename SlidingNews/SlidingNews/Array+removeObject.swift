//
//  Array+removeObject.swift
//  SlidingNews
//
//  Created by Pavel Karpov on 26.09.16.
//  Copyright © 2016 Pavel Karpov. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
