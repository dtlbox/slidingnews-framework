//
//  PageIndexCollectionViewController.swift
//  LazyPages
//
//  Created by Vargas Casaseca, Cesar on 24.03.16.
//  Copyright © 2016 WeltN24. All rights reserved.
//

import Foundation
import UIKit

public protocol PageIndexCollectionViewControllerDataSource: class {
  /**
   The cell to be shown at the the given index
   
   - parameter collectionView: the collection view where the index is represented
   - parameter indexPath: the index path of the requested cell
   */
  func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell
}

/// This class represents the index of the page controller, a view controller containing a collection view
open class PageIndexCollectionViewController: UIViewController {
  open weak var pageController: PageController?
  open weak var dataSource: PageIndexCollectionViewControllerDataSource?
    
    var backgroundColor: UIColor!
    var tabSpacing: CGFloat!
  
  @IBOutlet open weak var collectionView: UICollectionView!
  
  open override func viewDidLoad() {
    super.viewDidLoad()
    
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.backgroundColor = self.backgroundColor
    
    collectionView.isScrollEnabled = false
    
    guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
        return
    }
    flowLayout.estimatedItemSize = CGSize(width:1,height:1)
    flowLayout.minimumInteritemSpacing = 100
    flowLayout.minimumLineSpacing = self.tabSpacing
  }
}

extension PageIndexCollectionViewController: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
  public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    assert(pageController != nil, "The page controller reference in the PageIndexCollectionViewController cannot be nil")
    return pageController!.numberOfItems
  }
  
  public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    assert(dataSource != nil, "The data source of the PageIndexCollectionViewController cannot be nil")
    return dataSource!.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
  }
}

extension PageIndexCollectionViewController: UICollectionViewDelegate {
  public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let index = (indexPath as NSIndexPath).row
    pageController?.goToIndex(index)
    collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
  }
}
