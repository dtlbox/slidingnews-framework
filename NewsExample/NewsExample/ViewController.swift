//
//  ViewController.swift
//  NewsExample
//
//  Created by Pavel Karpov on 21.09.16.
//  Copyright © 2016 Pavel Karpov. All rights reserved.
//

import UIKit
import SlidingNews

class ViewController: UIViewController {

    var arrayDataSource: ArrayDataSource! = nil
    
    let customCellIdentifier        = "CustomTableViewCell"
    let customCodeCellIdentifier    = "CustomCodeTableViewCell"
    let bundle                      = "fork-pork.NewsExample"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // init with custom cell from nib true/false
        
        let newsVC = SlidingNewsController.factoryWithCell(fromNib: true,
                                            numberOfViewControllers: 6,
                                                          delegate: self,
                                                          cellIdentifier: self.customCellIdentifier,
                                                          bundle: self.bundle,
                                                          rowHeight: 44)
        
        newsVC.configureTabBarWith(backgroundColor: UIColor.black,
                                   textFont: UIFont.systemFont(ofSize: 16),
                                   textColor: UIColor.white,
                                   selectedTextColor: UIColor.blue,
                                   tabSpacing: 50.0,
                                   andTabTitles: ["Первая",
                                                  "Вторая",
                                                  "Третья",
                                                  "Четвертая",
                                                  "Пятая",
                                                  "Шестая"])
        
        // custom cell from nib
        
        let arrayDataSource = ArrayDataSource(cellIdentifier: self.customCellIdentifier) { (cell, item) in
                                                let cell   = cell as! CustomTableViewCell
                                                let item   = item as! String
            
                                                cell.customLabel.text = item
        }
        
        // custom cell programmatically
        
//        let arrayDataSource = ArrayDataSource(cellIdentifier: self.customCodeCellIdentifier) { (cell, item) in
//            let cell   = cell as! CustomCodeTableViewCell
//            let item   = item as! String
//
//            cell.customLabel.text = item
//        }
        
        self.arrayDataSource = arrayDataSource
        
        self.present(newsVC, animated: false, completion: nil)
    }
    
}

extension ViewController: SlidingNewsControllerDelegate {
    func willDisplayViewController(viewController: UITableViewController) {
        print(viewController.tableView.tag)
        self.arrayDataSource.selectedIndexes.removeAll()
        viewController.tableView.reloadData()
        viewController.tableView.delegate   = self.arrayDataSource
        viewController.tableView.dataSource = self.arrayDataSource
        if viewController.tableView.tag % 2 == 0 {
            self.arrayDataSource.items = ["Автором единственного забитого гола стал Юрий Голофаст.Гости из Уральска несмотря на не самый сильный состав произвели хорошее впечатление. И еще в первом тайме не раз могли открыть счет, но вратарь хозяев поля Вадим Бутенко на последнем рубеже действовал надежно. Нулевая ничья в первом тайме, команды продолжили упираться и во втором отрезке. Уже в компенсированное время, на 92-й минуте игры, Юрий Голофаст замкнул прострельную передачу Биржана Сагындыка и принес победу павлодарской молодежи - 1:0. - Главное, что ребята проявили характер, нашли в себе силы, забили победный гол в добавленное время. Решающую роль в данной игре сыграли индивидуальные действия Быржана Сагындык и Юрия Голофаста. Хочу поблагодарить всех ребят за самоотдачу, - заявил после матча Главный тренер дублирующего состава ФК «Иртыш» Андрей Кучерявых. Побороться за бронзовые медали молодежному сотаву \"Иртыша\" уже не получится. Костанайский Тобол с 43 баллами опережает павлодарцев на десять очков. <br>Артем Буланов <br>Фото пресс-службы ФК \"Иртыш\"<br><a target=\"_blank\" href=\"http://www.pavlodarnews.kz\">www.pavlodarnews.kz</a>;<br><br>" as NSString,"fork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-porkfork-pork" as NSString,"SlidingNewsController.factoryWithCellSlidingNewsController.factoryWithCellSlidingNewsController.factoryWithCellSlidingNewsController.factoryWithCellSlidingNewsController.factoryWithCellSlidingNewsController.factoryWithCell" as NSString]
        } else {
            self.arrayDataSource.items = ["111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111" as NSString,"Автором единственного забитого гола стал Юрий Голофаст.Гости из Уральска несмотря на не самый сильный состав произвели хорошее впечатление. И еще в первом тайме не раз могли открыть счет, но вратарь хозяев поля Вадим Бутенко на последнем рубеже действовал надежно. Нулевая ничья в первом тайме, команды продолжили упираться и во втором отрезке. Уже в компенсированное время, на 92-й минуте игры, Юрий Голофаст замкнул прострельную передачу Биржана Сагындыка и принес победу павлодарской молодежи - 1:0. - Главное, что ребята проявили характер, нашли в себе силы, забили победный гол в добавленное время. Решающую роль в данной игре сыграли индивидуальные действия Быржана Сагындык и Юрия Голофаста. Хочу поблагодарить всех ребят за самоотдачу, - заявил после матча Главный тренер дублирующего состава ФК «Иртыш» Андрей Кучерявых. Побороться за бронзовые медали молодежному сотаву \"Иртыша\" уже не получится. Костанайский Тобол с 43 баллами опережает павлодарцев на десять очков. <br>Артем Буланов <br>Фото пресс-службы ФК \"Иртыш\"<br><a target=\"_blank\" href=\"http://www.pavlodarnews.kz\">www.pavlodarnews.kz</a>;<br><br>" as NSString,"3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333" as NSString]
        }
    }
}
