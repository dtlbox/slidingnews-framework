//
//  CustomCodeTableViewCell.swift
//  NewsExample
//
//  Created by Pavel Karpov on 26.09.16.
//  Copyright © 2016 Pavel Karpov. All rights reserved.
//

import UIKit

class CustomCodeTableViewCell: UITableViewCell {

    let customLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        customLabel.frame = CGRect(x: 0, y: 0, width: 375, height: 44)
        self.addSubview(customLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
